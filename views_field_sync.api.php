<?php

/**
 * Hooks provided by Views field sync.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provide a way to alter field information before it gets added to a views display.
 *
 * @param $view
 *   The view that owns the display.
 * @param $display
 *   The display that will be modified.
 * @param $op
 *   The current operation being performed. Options are 'create instance',
 *   'update instance', 'delete instance'.
 * @param $field_column
 *   The database column name of the field being synchronized.
 * @param $table
 *   The table that contains the field being synchronized.
 * @param $options
 *   Options that will determine how the field behaves in the view.
 */
function hook_views_field_sync_alter(&$view, &$display, $op, $field_column, $table, &$options) {
  switch ($op) {
    case 'create instance':
      // Define a filter where the field is not empty
      $filter = array(
        'operator' => 'not empty',
      );
      // Add the field to the view
      $view->add_item($display->id, 'filter', $table, $field_column, $filter);
      break;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
